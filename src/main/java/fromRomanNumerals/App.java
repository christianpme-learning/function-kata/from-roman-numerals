package fromRomanNumerals;

public class App 
{
    public static void main( final String[] args) {
        
        System.out.println("Please input roman numerals:");
        String romanNumerals = System.console().readLine();

        App app = new App();
        int result = app.parse(romanNumerals);
        System.out.println("result: "+result);
    }

    public int parse(final String string) {

        return calculate(convert(string));
    }

    public int dictionary(final String string) {
        int number = 0;
        switch (string) {
        case "I":
            number = 1;
            break;
        case "V":
            number = 5;
            break;
        case "X":
            number = 10;
            break;
        case "L":
            number = 50;
            break;
        case "C":
            number = 100;
            break;
        case "D":
            number = 500;
            break;
        case "M":
            number = 1000;
            break;
        }
        return number;
    }

    public int[] convert(final String string) {
        final char[] numerals = string.toCharArray();
        final int[] numbers = new int[numerals.length];
        for (int i = 0; i < numerals.length; ++i) {
            numbers[i] = dictionary(String.valueOf(numerals[i]));
        }
        return numbers;
    }

    public int calculate(final int[] integers) {
        final int ARRAY_SIZE = integers.length;
        int result=0;
        int backwards=0;
        for(int i=0; i<ARRAY_SIZE;++i){

            final int currently = integers[i];
            int forward = 0;
            if(ARRAY_SIZE-1>i)
                forward=integers[i+1];

            if(backwards<currently){
                if(backwards!=0 || ARRAY_SIZE==1){
                    result += (currently-backwards);
                }
                else if(forward<=currently){
                    result += currently;
                }
            }
            else if(forward<=currently){
                result += currently;
            }
            backwards=currently;
        }
        return result;
    }
}
