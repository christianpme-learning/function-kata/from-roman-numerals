package fromRomanNumerals;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void parse_I_Test(){
        App app = new App();
        assertTrue(app.parse("I")==1);
    }

    @Test
    public void parse_V_Test(){
        App app = new App();
        assertTrue(app.parse("V")==5);
    }

    @Test
    public void parse_X_Test(){
        App app = new App();
        assertTrue(app.parse("X")==10);
    }

    @Test
    public void parse_L_Test(){
        App app = new App();
        assertTrue(app.parse("L")==50);
    }

    @Test
    public void parse_C_Test(){
        App app = new App();
        assertTrue(app.parse("C")==100);
    }

    @Test
    public void parse_D_Test(){
        App app = new App();
        assertTrue(app.parse("D")==500);
    }

    @Test
    public void parse_M_Test(){
        App app = new App();
        assertTrue(app.parse("M")==1000);
    }

    @Test
    public void parse_IV_Test(){
        App app = new App();
        assertTrue(app.parse("IV")==4);
    }

    @Test
    public void parse_IX_Test(){
        App app = new App();
        assertTrue(app.parse("IX")==9);
    }

    @Test
    public void parse_XLII_Test(){
        App app = new App();
        assertTrue(app.parse("XLII")==42);
    }

    @Test
    public void parse_XCIX_Test(){
        App app = new App();
        assertTrue(app.parse("XCIX")==99);
    }

    @Test
    public void parse_MMXIII_Test(){
        App app = new App();
        assertTrue(app.parse("MMXIII")==2013);
    }

}
